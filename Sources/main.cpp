#include <windows.h>
#include <commctrl.h>
#include <stdio.h>

#include "resource.h"

HINSTANCE hInst;

BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
        DragAcceptFiles(hwndDlg, TRUE);
        SetDlgItemTextA(hwndDlg, IDC_FILENAME, "Drag&Drop a file here");
        SetDlgItemTextA(hwndDlg, IDC_IMAGEBASE, "0000000000400000");
        SendDlgItemMessageA(hwndDlg, IDC_IMAGEBASE, EM_SETLIMITTEXT, 16, 0);
    }
    return TRUE;

    case WM_DROPFILES:
    {
        HDROP hDrop = (HDROP)wParam;
        char filename[MAX_PATH] = { 0x00 };
        DragQueryFileA(hDrop, 0, filename, MAX_PATH);
        SetDlgItemTextA(hwndDlg, IDC_FILENAME, filename);
        DragFinish(hDrop);
    }
    return TRUE;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, 0);
    }
    return TRUE;

    case WM_COMMAND:
    {
        switch(LOWORD(wParam))
        {
        case IDC_GO:
        {
            HANDLE infile;
            HANDLE outfile;

            unsigned int i;
            unsigned int file_size;
            unsigned long dwBytesWritten;
            unsigned long field3;
            unsigned long field4;

            char tablefile[FILENAME_MAX] = { 0x00 };
            char logfile[FILENAME_MAX] = { 0x00 };

            char *registers[] = {"rax", "rbx", "rcx", "rdx", "rsp", "rbp", "rsi", "rdi", "r8 ", "r9 ", "r10", "r11", "r12", "r13", "r14", "r15"};
            char imagebase[17] = { 0x00 };
            char log[1024] = { 0x00 };
            unsigned char VMEntry[0x0A] = { 0x00 };

            GetDlgItemTextA(hwndDlg, IDC_FILENAME, tablefile, FILENAME_MAX);
            GetDlgItemTextA(hwndDlg, IDC_IMAGEBASE, imagebase, 17);

            infile = CreateFile(tablefile, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

            file_size = GetFileSize(infile, NULL);
            if (!file_size || file_size % 0x0A != 0)
            {
                MessageBoxA(hwndDlg, "The VM table MUST have a size which is a multiple of 0x0A.\nCheck if you have correctly dumped it.", "Error", MB_ICONERROR);
                break;
            }

            i = strlen(tablefile);
            while(tablefile[i] != '\\')
            {
                i--;
            }
            memcpy(logfile, tablefile, i);

            strcat(logfile, "\\VMLog.txt");
            outfile = CreateFileA(logfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

            for(i = 0; i < file_size; i += 0x0A)
            {
                ReadFile(infile, VMEntry, 0x0A, &dwBytesWritten, NULL);

                switch(VMEntry[0])
                {
                case 0x00:
                {
                    field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                    field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                    if (VMEntry[1] == 0 && field3 == 0 && field4 == 0)
                    {
                        sprintf(log, "%08X:\tPossible end of VM proc\r\n", i);
                        break;
                    }
                    switch(VMEntry[2])
                    {
                    case 0x00:
                    {
                        sprintf(log, "%08X:\tpush %s\r\n", i, registers[(VMEntry[1])]);
                        break;
                    }
                    case 0xAA:
                    {
                        sprintf(log, "%08X:\tmov %s, %s\r\n", i, registers[(VMEntry[1])], registers[(VMEntry[3])]);
                        break;
                    }
                    case 0xBB:
                    {
                        sprintf(log, "%08X:\tadd %s, %s\r\n", i, registers[(VMEntry[1])], registers[(VMEntry[3])]);
                        break;
                    }
                    case 0xCC:
                    {
                        sprintf(log, "%08X:\tsub %s, %s\r\n", i, registers[(VMEntry[1])], registers[(VMEntry[3])]);
                        break;
                    }
                    case 0xDD:
                    {
                        switch (VMEntry[3])
                        {
                        case 0x01:
                        {
                            sprintf(log, "%08X:\tinc %s\r\n", i, registers[(VMEntry[1])]);
                            break;
                        }
                        case 0x02:
                        {
                            sprintf(log, "%08X:\tdec %s\r\n", i, registers[(VMEntry[1])]);
                            break;
                        }
                        default:
                        {
                            sprintf(log, "%08X:\tUnknown handler in VMEntry[3]: %02X\r\n", i, VMEntry[3]);
                            break;
                        }
                        }
                        break;
                    }
                    default:
                    {
                        sprintf(log, "%08X:\tUnknown handler in VMEntry[2]: %02X\r\n", i, VMEntry[2]);
                        break;
                    }
                    }
                    break;
                }
                case 0x01:
                {
                    sprintf(log, "%08X:\tpop %s\r\n", i, registers[(VMEntry[1])]);
                    break;
                }
                case 0x02:
                {
                    field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                    field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                    sprintf(log, "%08X:\tmov %s, %08X%08X\r\n", i, registers[(VMEntry[1])], field4, field3);
                    break;
                }
                case 0x03:
                {
                    field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                    field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                    if (field3 != 0 || field4 != 0)
                    {
                        sprintf(log, "%08X:\tcall %08X%08X\r\n", i, field4, field3);
                    }
                    else if (VMEntry[1] == 0)
                    {
                        sprintf(log, "%08X:\tcall rax\r\n", i);
                    }
                    else
                    {
                        sprintf(log, "%08X:\tsyscall\r\n", i);
                    }
                    break;
                }
                case 0x04:
                {
                    field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                    field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                    sprintf(log, "%08X:\tjmp %08X%08X\r\n", i, field4, field3);
                    break;
                }
                case 0x05:
                {
                    sprintf(log, "%08X:\txor %s, %s\r\n", i, registers[(VMEntry[1])], registers[(VMEntry[1])]);
                    break;
                }
                case 0x06:
                {
                    field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                    field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                    sprintf(log, "%08X:\tadd %s, %08X%08X\r\n", i, registers[(VMEntry[1])], field4, field3);
                    break;
                }
                case 0x07:
                {
                    if (VMEntry[1] != 0x04)
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\tsub %s, %08X%08X\r\n\t\tneg %s\r\n", i, registers[(VMEntry[1])], field4, field3, registers[(VMEntry[1])]);
                    }
                    else
                    {
                        sprintf(log, "%08X:\tnop\r\n", i);
                    }
                    break;
                }
                case 0x08:
                {
                    switch (VMEntry[1])
                    {
                    case 0x00:
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\ttest rax, rax\r\n\t\tjnz %08X%08X\r\n", i, field4, field3);
                        break;
                    }
                    case 0x01:
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\ttest rax, rax\r\n\t\tje %08X%08X\r\n", i, field4, field3);
                        break;
                    }
                    case 0x10:
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\ttest al, al\r\n\t\tjnz %08X%08X\r\n", i, field4, field3);
                        break;
                    }
                    default:
                    {
                        sprintf(log, "%08X:\tUnknown handler in VMEntry[1]: %02X\r\n", i, VMEntry[1]);
                        break;
                    }
                    }
                    break;
                }
                case 0x0B:
                {
                    sprintf(log, "%08X:\tmov %s, [%s]\r\n", i, registers[(VMEntry[1])], registers[(VMEntry[1])]);
                    break;
                }
                case 0x0C:
                {
                    field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                    field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                    sprintf(log, "%08X:\tmov [%08X%08X], %s\r\n", i, field4, field3, registers[(VMEntry[1])]);
                    break;
                }
                case 0x0D:
                {
                    switch (VMEntry[1])
                    {
                    case 0x00:
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\tand rax, %08X%08X\r\n", i, field4, field3);
                        break;
                    }
                    case 0x02:
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\tand rcx, %08X%08X\r\n", i, field4, field3);
                        break;
                    }
                    case 0x21:
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\tor  spl, %08X%08X\r\n", i, field4, field3);
                        break;
                    }
                    case 0x31:
                    {
                        switch (VMEntry[2])
                        {
                        case 0x01:
                        {
                            sprintf(log, "%08X:\tnop\r\n", i);
                            break;
                        }
                        default:
                        {
                            sprintf(log, "%08X:\tUnknown handler in VMEntry[2]: %02X\r\n", i, VMEntry[2]);
                            break;
                        }
                        }
                        break;
                    }
                    default:
                    {
                        sprintf(log, "%08X:\tUnknown handler in VMEntry[1]: %02X\r\n", i, VMEntry[1]);
                        break;
                    }
                    }
                    break;
                }
                case 0x0E:
                {
                    switch (VMEntry[1])
                    {
                    case 0x00:
                    {
                        sprintf(log, "%08X:\tnop\r\n", i);
                        break;
                    }
                    case 0x01:
                    {
                        sprintf(log, "%08X:\tmov rax, qword ptr gs:[30]\r\n", i);
                        break;
                    }
                    case 0x02:
                    {
                        sprintf(log, "%08X:\tmov rax, qword ptr gs:[60]\r\n", i);
                        break;
                    }
                    case 0x03:
                    {
                        sprintf(log, "%08X:\tmov rax, qword ptr gs:[30]\r\n", i);
                        break;
                    }
                    case 0x04:
                    case 0x05:
                    case 0x06:
                    case 0x07:
                    case 0x08:
                    case 0x09:
                    case 0x0A:
                    case 0x0B:
                    {
                        sprintf(log, "%08X:\tnop\r\n", i);
                        break;
                    }
                    default:
                    {
                        sprintf(log, "%08X:\tUnknown handler in VMEntry[1]: %02X\r\n", i, VMEntry[1]);
                        break;
                    }
                    }
                    break;
                }
                case 0x0F:
                {
                    switch (VMEntry[1])
                    {
                    case 0x01:
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\tsub rsp, %08X%08X\r\n", i, field4, field3);
                        break;
                    }
                    case 0x02:
                    {
                        field3 = VMEntry[5] * 0x1000000 + VMEntry[4] * 0x10000 + VMEntry[3] * 0x100 + VMEntry[2];
                        field4 = VMEntry[9] * 0x1000000 + VMEntry[8] * 0x10000 + VMEntry[7] * 0x100 + VMEntry[6];
                        sprintf(log, "%08X:\tadd rsp, %08X%08X\r\n", i, field4, field3);
                        break;
                    }
                    default:
                    {
                        sprintf(log, "%08X:\tUnknown handler in VMEntry[1]: %02X\r\n", i, VMEntry[1]);
                        break;
                    }
                    }
                    break;
                }
                default:
                {
                    sprintf(log, "%08X:\tUnknown handler in VMEntry[0]: %02X\r\n", i, VMEntry[0]);
                    break;
                }
                }
                WriteFile(outfile, log, strlen(log), &dwBytesWritten, NULL);
                SetFilePointer(outfile, 0, NULL, FILE_END);
            }
            CloseHandle(infile);
            CloseHandle(outfile);
        }
        }
    }
    return TRUE;
    }
    return FALSE;
}


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    hInst = hInstance;
    InitCommonControls();
    return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}
